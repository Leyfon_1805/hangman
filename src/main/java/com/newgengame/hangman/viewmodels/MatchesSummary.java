package com.newgengame.hangman.viewmodels;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.newgengame.hangman.core.Match;

import java.util.List;

public class MatchesSummary {
    @JsonProperty
    private long totalMathes;
    @JsonProperty
    private long inProgress;
    @JsonProperty
    private long won;
    @JsonProperty
    private long lost;

    private MatchesSummary(long inProgress, long won, long lost) {
        this.inProgress = inProgress;
        this.won = won;
        this.lost = lost;
        this.totalMathes = inProgress + won + lost;
    }

    public static MatchesSummary of(List<Match> matches) {
        long inProgress = matches.stream().filter(m -> m.isInProgress()).count();
        long lost = matches.stream().filter(m -> m.isLost()).count();
        long won = matches.stream().filter(m -> m.isWon()).count();
        return new MatchesSummary(inProgress, won, lost);
    }
}
