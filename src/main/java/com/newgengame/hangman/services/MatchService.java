package com.newgengame.hangman.services;

import com.newgengame.hangman.core.Match;
import com.newgengame.hangman.core.builders.MatchBuilder;
import com.newgengame.hangman.repositories.MatchRepository;
import com.newgengame.hangman.services.wordgenerators.WordGenerator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A service to manage complex logic of a Match.
 */
@Service
public class MatchService {
    private static final String IGNORED_CHARS_STRING = " -";
    private MatchRepository matchRepository;
    private WordGenerator wordGenerator;

    public MatchService(MatchRepository matchRepository, WordGenerator wordGenerator) {
        this.matchRepository = matchRepository;
        this.wordGenerator = wordGenerator;
    }

    @Transactional
    public Match createMatch() {
        String hiddenWord = wordGenerator.generate();
        MatchBuilder builder = new MatchBuilder(hiddenWord);
        builder.setIgnoredCharacter(IGNORED_CHARS_STRING);
        Match match = builder.build();

        //To be sure we are using attached match
        match = matchRepository.save(match);
        return match;
    }

    @Transactional
    /**
     * Insert a new Char into the match fetched by matchId and persist modifications.
     * The matchId must correspond a valid match.
     */
    public void insertNewChar(long matchId, char newChar) {
        Match match = matchRepository.findOne(matchId);
        match.insertCharacter(newChar);
    }

}
