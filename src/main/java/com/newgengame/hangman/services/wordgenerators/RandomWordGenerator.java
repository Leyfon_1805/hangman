package com.newgengame.hangman.services.wordgenerators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Simple implementation of WordGenerator.
 */
public class RandomWordGenerator implements WordGenerator {
    private final static List<String> WORDS;

    static {
        WORDS = new ArrayList<>();
        WORDS.add("NEBULOUS");
        WORDS.add("SCREW");
        WORDS.add("EFFECT");
        WORDS.add("TYPE");
        WORDS.add("PERFECT");
        WORDS.add("SURPRISE");
        WORDS.add("STRIPED");
        WORDS.add("DETERMINED");
        WORDS.add("RAMPANT");
        WORDS.add("SACK");
        WORDS.add("SATISFY");
        WORDS.add("CELERY");
        WORDS.add("TOMATOES");
        WORDS.add("SONG");
        WORDS.add("SPICY");
        WORDS.add("STIFF");
        WORDS.add("ACCOUNT");
        WORDS.add("VORACIOUS");
    }

    @Override
    public String generate() {
        List<String> copiedList = new ArrayList<>(WORDS);
        Collections.shuffle(copiedList);
        int randomIndex = (int) (Math.random() * 10000 % copiedList.size());
        return copiedList.get(randomIndex);
    }
}
