package com.newgengame.hangman.services.wordgenerators;

public interface WordGenerator {
    String generate();
}
