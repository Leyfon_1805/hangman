package com.newgengame.hangman.utils;

/**
 * Created by Leyfon on 2017/4/30.
 */
public class CharacterUtils {
    public static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static boolean isAlphabetCharacter(Character character) {
        return character != null && ALPHABET.contains(character.toString().toUpperCase());
    }
}
