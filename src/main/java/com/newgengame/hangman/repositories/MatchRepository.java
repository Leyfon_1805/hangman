package com.newgengame.hangman.repositories;

import com.newgengame.hangman.core.Match;
import org.springframework.data.repository.CrudRepository;

public interface MatchRepository extends CrudRepository<Match, Long> {
}
