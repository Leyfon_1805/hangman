package com.newgengame.hangman.controllers.exceptions;

/**
 * Created by Leyfon on 2017/5/3.
 */
public class PreconditionFailedException extends RuntimeException {
    public PreconditionFailedException(String s) {
        super(s);
    }
}
