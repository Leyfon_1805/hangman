package com.newgengame.hangman.controllers;

import com.newgengame.hangman.controllers.exceptions.BadRequestException;
import com.newgengame.hangman.controllers.exceptions.NotFoundException;
import com.newgengame.hangman.controllers.exceptions.PreconditionFailedException;
import com.newgengame.hangman.core.Match;
import com.newgengame.hangman.repositories.MatchRepository;
import com.newgengame.hangman.services.MatchService;
import com.newgengame.hangman.utils.CharacterUtils;
import com.newgengame.hangman.viewmodels.MatchesSummary;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/match")
@Slf4j
/**
 * A REST controller for Match entity.
 * Match is returned without using a view-model to avoid unnecessary parsing.
 */
public class MatchController {
    private MatchService matchService;
    private MatchRepository matchRepository;

    public MatchController(MatchService matchService, MatchRepository matchRepository) {
        this.matchService = matchService;
        this.matchRepository = matchRepository;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Match createMatch() {
        return matchService.createMatch();
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Match> findAllMatches() {
        return (List<Match>) matchRepository.findAll();
    }

    @RequestMapping(value = "/summary", method = RequestMethod.GET)
    public MatchesSummary summary() {
        List<Match> allMatches = (List<Match>) matchRepository.findAll();
        return MatchesSummary.of(allMatches);
    }


    @RequestMapping(value = "/{matchId}", method = RequestMethod.GET)
    public Match findMatch(@PathVariable(value = "matchId") Long matchId) {
        Match match = matchRepository.findOne(matchId);
        if (match == null) {
            log.trace("Requested a match of {}, but this id not exist", matchId);
            throw new NotFoundException("Doesn't exist a match with this id: " + matchId);
        }
        return match;
    }

    @RequestMapping(value = "/{matchId}/insertChar/{newChar}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void insertNewChar(@PathVariable(value = "matchId") Long matchId, @PathVariable(value = "newChar") Character newChar) {
        if (!CharacterUtils.isAlphabetCharacter(newChar)) {
            log.trace("Requested to insert a invalid character {} in match {}", newChar, matchId);
            throw new BadRequestException("INVALID_CHAR");
        }

        Match match = matchRepository.findOne(matchId);
        if (match == null) {
            log.trace("Requested to insert a character into not existing matchId {}", matchId);
            throw new NotFoundException("Doesn't exist a match with this id: " + matchId);
        } else if (match.isComplete()) {
            log.trace("Requested to insert a character into a completed match");
            throw new PreconditionFailedException("Match is alread complete");
        }

        matchService.insertNewChar(matchId, newChar);

    }
}
