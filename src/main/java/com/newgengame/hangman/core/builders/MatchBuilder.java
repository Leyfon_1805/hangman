package com.newgengame.hangman.core.builders;

import com.newgengame.hangman.core.Match;
import lombok.Setter;

public class MatchBuilder {
    private String hiddenWord;
    @Setter
    private String characterMarker = "_";
    @Setter
    private String ignoredCharacter;

    public MatchBuilder(String hiddenWord) {
        this.hiddenWord = hiddenWord.toUpperCase();
        this.ignoredCharacter = "";
    }

    public Match build() {
        String revealedCharacterStatus = createRevealedStatus(hiddenWord);
        return new Match(hiddenWord, revealedCharacterStatus, 9);
    }


    private String createRevealedStatus(String hiddenWord) {
        StringBuilder revealedBuilder = new StringBuilder();
        for (int i = 0; i < hiddenWord.length(); i++) {
            char c = hiddenWord.charAt(i);
            if (isIgnoredChar(c)) {
                revealedBuilder.append(c);
            } else {
                revealedBuilder.append(characterMarker);
            }
        }
        return revealedBuilder.toString();
    }

    private boolean isIgnoredChar(char c) {
        return ignoredCharacter.toUpperCase().contains(Character.toString(c));
    }
}
