package com.newgengame.hangman.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


/**
 * A rapresentation of a hangman Match.
 * All field can be modify only using insertCharacter().
 * The serializzation of a match using jackson will have same data accessble by getter.
 */
@Entity
@Table(name = "MATCH")
public class Match {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "HANGMAN_SEQ")
    @SequenceGenerator(name = "HANGMAN_SEQ", sequenceName = "HANGMAN_SEQ")
    @Column(name = "MATCH_ID")
    @Getter
    private Long id;

    @Column(name = "HIDDEN_WORD")
    @JsonIgnore
    private String hiddenWord;

    @Column(name = "REVEALED_CHAR_STATUS")
    @Getter
    private String revealedCharacterStatus;

    @Column(name = "ERRORS_PERMETED")
    @Getter
    private Long errorsPermeted = 9L;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "match")
    @Getter
    @JsonManagedReference
    private Set<InsertedCharacter> insertedCharacters;

    private Match() {
    }

    public Match(String hiddenWord, String initianlRevealedCharacterStatus, long errorsPermited) {
        if(hiddenWord.length() != initianlRevealedCharacterStatus.length()){
            throw new IllegalArgumentException("revealedCharacterStatus must be length equals with hiddenWord lenth");
        }
        this.hiddenWord = hiddenWord.toUpperCase();
        this.revealedCharacterStatus = initianlRevealedCharacterStatus;
        this.insertedCharacters = new HashSet<>();
        this.errorsPermeted = errorsPermited;
    }

    /**
     * Insert a new character into the match. Case Insensitive.
     * A character must be a alphabet character.
     * Used character will be ignored.
     * If the match is complete all characters will be ignored.
     *
     * @param character any alphabet character. Upper or lower case.
     */
    public void insertCharacter(Character character) {
        if (!isComplete()) {
            Character characterUpperCase = Character.toUpperCase(character);
            if (hiddenWord.indexOf(characterUpperCase) >= 0) {
                insertedCharacters.add(InsertedCharacter.correct(this, characterUpperCase));
                updateRevealedCharacterStatus(characterUpperCase);
            } else {
                insertedCharacters.add(InsertedCharacter.wrong(this, characterUpperCase));
            }
        }
    }


    public boolean isComplete() {
        return isLost() || isWon();
    }

    public boolean isInProgress() {
        return !isComplete();
    }

    public boolean isLost() {
        return countErrors() >= errorsPermeted;
    }

    public boolean isWon() {
        return countRemainingPositions() == 0;
    }

    @JsonProperty("errorsCounter")
    public long countErrors() {
        return insertedCharacters.stream().filter(insertedCharacter -> !insertedCharacter.isCorrect()).count();
    }

    /**
     * Returns the hiddenWord only after the match is complete, otherwise returns null.
     */
    @JsonProperty("hiddenWord")
    public String getHiddenWord() {
        if (isComplete()) {
            return hiddenWord;
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return revealedCharacterStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Match match = (Match) o;

        return id == match.id;
    }

    private void updateRevealedCharacterStatus(char c) {
        StringBuilder revealedBuilder = new StringBuilder(revealedCharacterStatus);
        for (int i = 0; i < hiddenWord.length(); i++) {
            if (hiddenWord.charAt(i) == c) {
                revealedBuilder.setCharAt(i, c);
            }
        }
        revealedCharacterStatus = revealedBuilder.toString();
    }

    private long countRemainingPositions() {
        long counter = 0;
        for (int i = 0; i < hiddenWord.length(); i++) {
            if (hiddenWord.charAt(i) != revealedCharacterStatus.charAt(i)) {
                counter++;
            }
        }
        return counter;
    }
}
