package com.newgengame.hangman.core;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;

import javax.persistence.*;


@Entity
@Table(name = "match_inserted_character")
/**
 * All Character inserted by users. That will be useful to create reports or analytics.
 */
public class InsertedCharacter {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "HANGMAN_SEQ")
    @SequenceGenerator(name = "HANGMAN_SEQ", sequenceName = "HANGMAN_SEQ")
    @Column(name = "INSERTED_CHARACTER_ID")
    @Getter
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MATCH_ID")
    @Getter
    @JsonBackReference
    private Match match;

    @Column(name = "CHARACTER")
    @Getter
    private Character character;

    @Column(name = "CORRECT")
    private Boolean correct;

    private InsertedCharacter() {
    }

    private InsertedCharacter(Match match, char character, Boolean correct) {
        this.match = match;
        this.character = character;
        this.correct = correct;
    }

    public static InsertedCharacter correct(Match match, char character) {
        return new InsertedCharacter(match, character, true);
    }

    public static InsertedCharacter wrong(Match match, char character) {
        return new InsertedCharacter(match, character, false);
    }

    public boolean isCorrect() {
        return correct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InsertedCharacter that = (InsertedCharacter) o;

        if (match != null ? !match.equals(that.match) : that.match != null) return false;
        return character != null ? character.equals(that.character) : that.character == null;
    }
}
