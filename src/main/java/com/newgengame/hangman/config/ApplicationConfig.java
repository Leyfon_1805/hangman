package com.newgengame.hangman.config;

import com.newgengame.hangman.services.wordgenerators.RandomWordGenerator;
import com.newgengame.hangman.services.wordgenerators.WordGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {
    @Bean
    public WordGenerator wordGenerator() {
        //It can be replace with more complex generator implementation in anytime.
        //A idea was a generator using same online dictionary api.
        return new RandomWordGenerator();
    }

}
