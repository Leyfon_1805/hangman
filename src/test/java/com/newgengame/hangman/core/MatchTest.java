package com.newgengame.hangman.core;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class MatchTest {
  @Test
  public void test_a_won_match() throws Exception {
    String word = "TEST";
    String initianlRevealedCharacterStatus= "****";
    int errorExpected = 2;
      Match match = new Match(word, initianlRevealedCharacterStatus, 9);

    match.insertCharacter('A');
    match.insertCharacter('B');
    match.insertCharacter('T');
    match.insertCharacter('E');
    match.insertCharacter('S');

    assertThat(match.countErrors()).isEqualTo(errorExpected);
    assertThat(match.isComplete()).isEqualTo(true);
    assertThat(match.toString()).isEqualToIgnoringCase(word);
    assertThat(match.isWon()).isEqualTo(true);
    System.out.println(match.toString());
  }

  @Test
  public void test_a_lost_match() throws Exception {
    String word = "TEST";
    String initianlRevealedCharacterStatus= "****";
    int errorExpected = 9;
      Match match = new Match(word, initianlRevealedCharacterStatus, 9);

    match.insertCharacter('A');
    match.insertCharacter('b');
    match.insertCharacter('c');
    match.insertCharacter('d');
    match.insertCharacter('e');
    match.insertCharacter('f');
    match.insertCharacter('g');
    match.insertCharacter('h');
    match.insertCharacter('i');
    match.insertCharacter('j');

    assertThat(match.countErrors()).isEqualTo(errorExpected);
    assertThat(match.isComplete()).isEqualTo(true);
    assertThat(match.toString()).isNotEqualToIgnoringCase(word);
    assertThat(match.isWon()).isEqualTo(false);
    System.out.println(match.toString());
  }

//  @Test(expected = MatchIsAlreadyCompleteException.class)
//  public void test_insert_chars_in_completed_match() throws Exception {
//    Match match = new Match("", "");
//
//    match.insertCharacter('A');
//  }

  @Test
  public void test_multi_word_won_match() throws Exception {
    String phrase = "THIS IS A TEST";
    String initianlRevealedCharacterStatus = "**** -- % ~~~~";
    char[] rightChars = {'A', 'S', 'T', 'E', 'H', 'I'};
    int errorExpected = 0;
      Match match = new Match(phrase, initianlRevealedCharacterStatus, 9);

    for (char rightChar : rightChars) {
      match.insertCharacter(rightChar);
    }

    assertThat(match.countErrors()).isEqualTo(errorExpected);
    assertThat(match.isComplete()).isEqualTo(true);
    assertThat(match.isWon()).isEqualTo(true);
    System.out.println(match.toString());
  }

  @Test
  public void test_multi_word_lost_match() throws Exception {
    String phrase = "THIS IS A TEST";
    String initianlRevealedCharacterStatus = "**** -- % ~~~~";
    int errorExpected = 9;
      Match match = new Match(phrase, initianlRevealedCharacterStatus, 9);

    match.insertCharacter('A');
    match.insertCharacter('b');
    match.insertCharacter('c');
    match.insertCharacter('d');
    match.insertCharacter('e');
    match.insertCharacter('f');
    match.insertCharacter('g');
    match.insertCharacter('h');
    match.insertCharacter('i');
    match.insertCharacter('j');
    match.insertCharacter('k');
    match.insertCharacter('l');
    match.insertCharacter('m');

    assertThat(match.countErrors()).isEqualTo(errorExpected);
    assertThat(match.isComplete()).isEqualTo(true);
    assertThat(match.isWon()).isEqualTo(false);
    System.out.println(match.toString());
  }

  @Test
  public void test_in_progress_match() throws Exception {
    String phrase = "THIS IS A TEST";
    String initianlRevealedCharacterStatus = "**** -- % ~~~~";
    int errorExpected = 5;
      Match match = new Match(phrase, initianlRevealedCharacterStatus, 9);

    match.insertCharacter('P');
    match.insertCharacter('O');
    match.insertCharacter('T');
    match.insertCharacter('L');
    match.insertCharacter('X');
    match.insertCharacter('A');
    match.insertCharacter('Y');

    assertThat(match.countErrors()).isEqualTo(errorExpected);
    assertThat(match.isComplete()).isEqualTo(false);
    assertThat(match.isInProgress()).isEqualTo(true);
    System.out.println(match.toString());
  }

}
