package com.newgengame.hangman.core;

import org.junit.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;

public class InsertedCharacterTest {
    @Test
    public void not_equals() throws Exception {
        Match match = Mockito.mock(Match.class);
        InsertedCharacter a = InsertedCharacter.correct(match, 'A');
        InsertedCharacter b = InsertedCharacter.correct(match, 'B');

        assertThat(a).isNotEqualTo(b);
    }

    @Test
    public void is_equals() throws Exception {
        Match match = Mockito.mock(Match.class);
        InsertedCharacter a = InsertedCharacter.correct(match, 'A');
        InsertedCharacter b = InsertedCharacter.correct(match, 'A');

        assertThat(a).isEqualTo(b);
    }
}