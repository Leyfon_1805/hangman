package com.newgengame.hangman.core.builders;

import com.newgengame.hangman.core.Match;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

public class MatchBuilderTest {
    @Test
    public void default_marker() throws Exception {
        String word = "Banana";
        MatchBuilder matchBuilder = new MatchBuilder(word);

        Match match = matchBuilder.build();

        assertThat(match.getRevealedCharacterStatus()).isEqualTo("______");
    }

    @Test
    public void ignore_some_chars() throws Exception {
        String ignoredChars = "a - !";
        String word = "Banana long-term";
        MatchBuilder matchBuilder = new MatchBuilder(word);
        matchBuilder.setIgnoredCharacter(ignoredChars);

        Match match = matchBuilder.build();

        assertThat(match.getRevealedCharacterStatus()).isEqualTo("_A_A_A ____-____");
    }

    @Test
    public void ignore_some_chars_with_custom_marker() throws Exception {
        String ignoredChars = "a - !";
        String word = "Banana long-term";
        MatchBuilder matchBuilder = new MatchBuilder(word);
        matchBuilder.setIgnoredCharacter(ignoredChars);
        matchBuilder.setCharacterMarker("*");

        Match match = matchBuilder.build();

        assertThat(match.getRevealedCharacterStatus()).isEqualTo("*A*A*A ****-****");
    }
}