package com.newgengame.hangman.services;

import com.newgengame.hangman.core.Match;
import com.newgengame.hangman.repositories.MatchRepository;
import com.newgengame.hangman.services.wordgenerators.WordGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;

@RunWith(MockitoJUnitRunner.class)
public class MatchServiceTest {
    @Mock
    private WordGenerator wordGenerator;
    @Mock
    private MatchRepository matchRepository;
    @InjectMocks
    private MatchService matchService;

    @Test
    public void test_create_a_match() throws Exception {
        String generatedWord = "TEST";
        Mockito.when(wordGenerator.generate()).thenReturn(generatedWord);
        Match attachedMatch = Mockito.mock(Match.class);
        Mockito.when(matchRepository.save(any(Match.class))).thenReturn(attachedMatch);

        Match match = matchService.createMatch();

        assertThat(match).isNotNull();
        assertThat(match).isEqualTo(attachedMatch);
        Mockito.verify(matchRepository, Mockito.times(1)).save(any(Match.class));
    }
}