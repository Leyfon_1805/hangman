package com.newgengame.hangman.services.wordgenerators;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RandomWordGeneratorTest {

    private WordGenerator service;

    @Before
    public void setUp() throws Exception {
        service = new RandomWordGenerator();
    }

    @Test
    public void test_generate() throws Exception {
        for (int i = 0; i < 10000; i++) {
            String word = service.generate();
            assertThat(word).isNotNull();
            System.out.println(word);
        }
    }
}