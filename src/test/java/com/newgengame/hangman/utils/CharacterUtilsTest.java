package com.newgengame.hangman.utils;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CharacterUtilsTest {
    @Test
    public void test_isAlphabetCharacter_invalid() throws Exception {
        assertThat(CharacterUtils.isAlphabetCharacter('1')).isEqualTo(false);
        assertThat(CharacterUtils.isAlphabetCharacter('!')).isEqualTo(false);
        assertThat(CharacterUtils.isAlphabetCharacter('\r')).isEqualTo(false);
        assertThat(CharacterUtils.isAlphabetCharacter('(')).isEqualTo(false);
        assertThat(CharacterUtils.isAlphabetCharacter(null)).isEqualTo(false);
    }

    @Test
    public void test_isAlphabetCharacter_valid() throws Exception {
        assertThat(CharacterUtils.isAlphabetCharacter('a')).isEqualTo(true);
        assertThat(CharacterUtils.isAlphabetCharacter('A')).isEqualTo(true);
        assertThat(CharacterUtils.isAlphabetCharacter('b')).isEqualTo(true);
        assertThat(CharacterUtils.isAlphabetCharacter('T')).isEqualTo(true);
    }
}