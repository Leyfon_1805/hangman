const conf = require('./gulp.conf');
var url = require('url');
var proxy = require('proxy-middleware');

module.exports = function () {
    var proxyOptions = url.parse('http://localhost:8080/api');
    proxyOptions.route = '/api';
    return {
        server: {
            baseDir: [
                conf.paths.tmp,
                conf.paths.src
            ],
            routes: {
                '/bower_components': 'bower_components'
            }
        },
        middleware: [proxy(proxyOptions)],
        open: false
    };
};
