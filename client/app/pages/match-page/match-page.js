angular
    .module('app')
    .component('matchPage', {
        templateUrl: 'app/pages/match-page/match-page.html',
        controller: 'matchPageCtrl as ctrl'
    });
