"use strict";
angular
    .module('app')
    .controller('matchPageCtrl', matchPageCtrl);

/** @ngInject */
function matchPageCtrl(MatchService, $stateParams, $state) {
    var ctrl = this;
    ctrl.keyboard = generateKeyboardCharacters();
    ctrl.matchId = $stateParams.matchId;
    ctrl.match = {};
    ctrl.errors = 0;
    ctrl.maxErrors = 5;
    ctrl.insertCharacter = insertCharacter;
    ctrl.newMatch = newMatch;
    loadMatch();


    function newMatch() {
        MatchService.createMatch().then(function (match) {
            $state.go("match", {"matchId": match.id});
        });
    }

    function insertCharacter(char) {
        MatchService.insertCharacterToMatch(char, ctrl.matchId)
            .then(function (response) {
                loadMatch();
            });
    }

    function loadMatch() {
        MatchService.getMatch(ctrl.matchId)
            .then(function (match) {
                ctrl.match = match;
                ctrl.errors = countErrors();
                updateKeyboard();
                console.log(ctrl.keyboard);
            });
    }

    function countErrors() {
        var errors = 0;
        for (var i in ctrl.match.insertedCharacters) {
            if (!ctrl.match.insertedCharacters[i].correct) {
                errors++;
            }
        }
        return errors;
    }

    function updateKeyboard() {
        for (var i in ctrl.match.insertedCharacters) {
            markKeyboardKey(ctrl.match.insertedCharacters[i]);
        }
    }

    function markKeyboardKey(insertedChar) {
        for (var i in ctrl.keyboard) {
            var key = ctrl.keyboard[i];
            if (key.character == insertedChar.character) {
                key.used = true;
                key.correct = insertedChar.correct;
            }
        }
    }

    function generateKeyboardCharacters() {
        var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        var keyboard = [];
        for (var i in characters) {
            var key = {
                "character": characters[i],
                "used": false
            };
            keyboard.push(key);
        }
        return keyboard;
    }
}
