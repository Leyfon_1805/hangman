angular
  .module('app')
    .component('main', {
        templateUrl: 'app/pages/main/main.html',
        controller: 'mainCtrl as ctrl'
  });
