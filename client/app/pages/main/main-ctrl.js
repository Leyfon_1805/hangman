"use strict";
angular
    .module('app')
    .controller('mainCtrl', mainCtrl);

/** @ngInject */
function mainCtrl(MatchService, $state, $uibModal) {
    var ctrl = this;
    ctrl.newMatch = newMatch;
    ctrl.openContinueModal = openContinueModal;
    ctrl.summary = {};
    ctrl.labels = ["In progress", "Won", "Lost"];
    ctrl.data = [];
    ctrl.options = {legend: {display: true}};
    laodSummary();

    function laodSummary() {
        MatchService.summary().then(function () {
            MatchService.summary()
                .then(function (summary) {
                    ctrl.summary = summary;
                    ctrl.data = [];
                    ctrl.data.push(summary.inProgress);
                    ctrl.data.push(summary.won);
                    ctrl.data.push(summary.lost);
                });
        });
    }

    function newMatch() {
        MatchService.createMatch().then(function (match) {
            $state.go("match", {"matchId": match.id});
        });
    }

    function openContinueModal() {
        $uibModal.open({
            // ariaLabelledBy: 'modal-title-top',
            // ariaDescribedBy: 'modal-body-top',
            templateUrl: 'app/components/select-match-modal/select-match-modal.html',
            size: 'sm',
            controller: "selectMatchModalCtrl as ctrl"
            // ,
            // controller: function($scope) {
            //     $scope.name = 'top';
            // }
        }).result.then(function (matchId) {
            $state.go("match", {"matchId": matchId});
        });
    };


}
