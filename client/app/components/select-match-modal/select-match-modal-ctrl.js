"use strict";
angular
    .module('app')
    .controller('selectMatchModalCtrl', selectMatchModalCtrl);

/** @ngInject */
function selectMatchModalCtrl($uibModalInstance) {
    var ctrl = this;
    ctrl.matchId;

    ctrl.ok = function () {
        $uibModalInstance.close(ctrl.matchId);
    };

    ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}
