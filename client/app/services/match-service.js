angular
    .module('app')
    .service('MatchService', MatchService);

/** @ngInject */
function MatchService($http) {
    var baseUrl = "/api/match/";
    return service = {
        insertCharacterToMatch: insertCharacterToMatch,
        getMatch: getMatch,
        createMatch: createMatch,
        summary: summary
    };

    function insertCharacterToMatch(char, matchId) {
        var body = {};
        return $http.put(
            baseUrl + matchId + "/insertChar/" + char,
            body
        ).then(function (response) {
            return response;
        });
    }

    function getMatch(matchId) {
        return $http
            .get(baseUrl + matchId)
            // .get("/mocks/MatchMock.json")
            .then(function (response) {
                return response.data;
            });
    }

    function summary() {
        return $http
            .get(baseUrl + "summary/")
            .then(function (response) {
                return response.data;
            });
    }

    function createMatch() {
        return $http
            .post(baseUrl, {})
            .then(function (response) {
                return response.data;
            });
    }
}